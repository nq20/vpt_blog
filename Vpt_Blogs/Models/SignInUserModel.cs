﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class SignInUserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string FirstName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class Response
    {
        public bool Success { get; set; }
        public object ErrorMessage { get; set; }
        public object Data { get; set; }
    }
}
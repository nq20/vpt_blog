﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class BlogDetailsModel
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ShortDesc { get; set; }
        public string ImagePath { get; set; }
        public string KeywordContent { get; set; }
        public string MetaContent { get; set; }
        public string AltTags { get; set; }
        public string OldKey { get; set; }
        public List<PopularPosts> popularPosts { get; set; }
        public List<PopularCategory> popularCategories { get; set; }
        public List<PopularTags> popularTags { get; set; }
        //public List<AuthorMasterList> AuthorList { get; set; }
        public string Designation { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string LinkedInUrl { get; set; }
        public string AboutAuthor { get; set; }
        public string imageFile { get; set; }
        public HttpPostedFileBase Image { get; set; }


    }

    public class PopularPosts {
        public string ImagePath { get; set; }
        public string Title { get; set; }
        public string Tags { get; set; }
        public string Urlkey { get; set; }

        public List<PopularPosts> popularPosts { get; set; }

    }

    public class PopularCategory { 
        public string Category { get; set; }
        public List<PopularCategory> popularCategories { get; set; }
    }

    public class PopularTags { 
       public string Tag { get; set; }
       public List<PopularTags> popularTags { get; set; }
    }

    public class BlogKeyModel
    {
        public string OldKey { get; set; }
        public string CurrentKey { get; set; }
    }
}
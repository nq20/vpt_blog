﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class WhitePaperModel
    {
        public int Id { get; set; }
        
        public HttpPostedFileBase Image { get; set; }
        public List<WhitePaperModel> WhitePaperListModels { get; set; }
    }
}
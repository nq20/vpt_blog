﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class AddBlogModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Title is required.")]
        public string title { get; set; }
        [Required(ErrorMessage = "Custom URL is required.")]
        public string key { get; set; }
        [Required(ErrorMessage = "Meta Description is required.")]
        public string metaDescription { get; set; }
        [Required(ErrorMessage = "Keywords is required.")]
        public string metaKeywords { get; set; }
        [Required(ErrorMessage = "Short Description is required.")]
        public string shortDesc { get; set; }
        [Required(ErrorMessage = "Select Posted By.")]
        public int authorId { get; set; }
        //[Required(ErrorMessage = "Select Image File.")]
        public HttpPostedFileBase imagePath { get; set; }
        [Required(ErrorMessage = "Select Category Type.")]
        public int categoryId { get; set; }
        [Required(ErrorMessage = "Tags is required.")]
        public string tags { get; set; }
        [Required(ErrorMessage = "Image Tags is required.")]
        public string imageTag { get; set; }
        [Required(ErrorMessage = "Description is required.")]
        public string description { get; set; }
        public bool? status { get; set; }
        public DateTime createdDate { get; set; }
        public string imageFile { get; set; }
        public string author { get; set; }
        public List<AuthorMasterList> AuthorList { get; set; }
        public List<CategoryMasterList> CategoryList { get; set; }
    }

    public class StatusInputModel
    {
        public int id { get; set; }
        public string status { get; set; }
    }

    public class AuthorMasterList
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        public string Designation { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string LinkedInUrl { get; set; }
        public string AboutAuthor { get; set; }
        public string imageFile { get; set; }
        public string Description { get; set; }
        public string Profile { get; set; }
        public bool? Status { get; set; }
        public HttpPostedFileBase Image { get; set; }
        
        public List<AuthorMasterList> AuthorList { get; set; }
        
    }

    public class CategoryMasterList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Status { get; set; }
        //public List<CategoryMasterList> CategoryList { get; set; }

    }
}
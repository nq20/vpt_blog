﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class GetVptBlogModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Category is required.")]
        public string Category { get; set; }
        public bool? Status { get; set; }
        public List<CategoryMasterList> CategoryList { get; set; }
    }
}
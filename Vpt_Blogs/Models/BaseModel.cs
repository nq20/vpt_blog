﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            this.CreatedOn = this.UpdatedOn = DateTime.Now;
        }

        public int Id { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }
        public Guid UpdatedBy { get; set; }

        public string CreatedByUserName { get; set; }
        public string UpdatedByUserName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class UserModel : BaseModel
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PasswordUpdate { get; set; }
    }

    public class VptSendMail
    {
        public string fullName { get; set; }
        public string emailAddress { get; set; }
        public string mobileNumber { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public string countryName { get; set; }
        public string companyName { get; set; }
        public string designation { get; set; }
    }

    public class JobApplicationModel
    {
        public string fullName { get; set; }
        public string emailAddress { get; set; }
        public string mobileNumber { get; set; }
        public string linkedinProfile { get; set; }
        public string position { get; set; }
        public string details { get; set; }
        public string resume { get; set; }
    }
}
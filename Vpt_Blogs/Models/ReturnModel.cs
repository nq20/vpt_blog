﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class ReturnModel
    {
        public HttpStatusCode StatusCode { get; set; }
        public string message { get; set; } = "Successfull";
        public dynamic data { get; set; }
    }
}
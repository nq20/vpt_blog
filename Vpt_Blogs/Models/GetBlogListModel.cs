﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vpt_Blogs.Models
{
    public class GetBlogListModel
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string ShortDesc { get; set; }
        public string ImagePath { get; set; }
        public string Urlkey { get; set; }

        public List<GetBlogListModel> blogListModels { get; set; }
    }
}
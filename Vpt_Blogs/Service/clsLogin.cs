﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Vpt_Blogs.Models;

namespace Vpt_Blogs.Service
{
    public class clsLogin
    {
        #region Use for Get User data
        /// <summary>
        /// Use for Get User data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static DataTable GetUserdata(SignInUserModel model)
        {
            SqlParameter[] param =
           {
                new SqlParameter("@Email",model.Email),
                new SqlParameter("@UserName",model.UserName),
                new SqlParameter("@Password",model.Password)
            };
            return DAL.DAL.GenerateDataTable("USP_GetUserWithUsername", CommandType.StoredProcedure, param);
        }
        #endregion

        #region Use For Get User
        /// <summary>
        /// Use For Get User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DataTable GetUser(SignInUserModel model)
        {
            SqlParameter[] param =
           {
                new SqlParameter("@Email",model.Email),
                new SqlParameter("@UserName",model.UserName),
                new SqlParameter("@Password",model.Password)
            };
            return DAL.DAL.GenerateDataTable("USP_GetUserWithUsername", CommandType.StoredProcedure, param);
        }
        #endregion

        #region use for User Validation
        /// <summary>
        /// use for User Validation
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Response IsValid(string userName, string password)
        {
            Response response = new Response();
            try
            {
                UserModel user = new UserModel();
                user = GetUser(userName, password);
                if (user != null)
                {
                    if (password == user.PasswordUpdate)
                    {
                        response.Success = true;
                        response.Data = user;
                    }
                    else
                    {
                        response.Success = false;
                        response.ErrorMessage = "Incorrect password";
                    }
                }
                else
                {
                    response.Success = false;
                    response.ErrorMessage = "Incorrect Username";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Data = ex.InnerException;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }
        #endregion

        #region Use For fetch User Details
        /// <summary>
        /// Use For fetch User Details
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public UserModel GetUser(string UserName, string password)
        {
            UserModel user = new UserModel();
            SqlParameter[] param =
                    {
                        new SqlParameter("@UserName",UserName),
                        new SqlParameter("@Password",password)
                    };

            DataTable dt = DAL.DAL.GenerateDataTable("USP_GetUserWithUsername", CommandType.StoredProcedure, param);
            user = (from DataRow row in dt.Rows
                    select new UserModel
                    {
                        // todo 
                        Id = Convert.ToInt32(row["Id"]),
                        Email = row["Email"].ToString(),
                        UserName = row["UserName"].ToString(),
                        RoleId = Convert.ToInt32(row["RoleId"]),
                        PasswordUpdate = row["Password"].ToString(),
                    }).FirstOrDefault();
            return user;
        }
        #endregion
    }
}
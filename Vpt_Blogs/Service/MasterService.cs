﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Vpt_Blogs.Models;

namespace Vpt_Blogs.Service
{
    public class MasterService
    {
        #region use for Add Category
        /// <summary>
        /// use for Add Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddCategory(GetVptBlogModel model)
        {
            bool result = false;
            string option = "insert";
            try
            {
                SqlParameter[] param =
                        {   new SqlParameter ("@option", option),
                            new SqlParameter ("@Category", model.Category),
                            new SqlParameter ("@Status", model.Status),
                        };
                result = DAL.DAL.ExecuteNonQuery("USP_InsertUpdateCategory", CommandType.StoredProcedure, param);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Use for Change Author Status
        /// <summary>
        /// Use for Change Author Status
        /// </summary>
        /// <returns></returns>
        public bool ChangeAuthorStatus(int Id, int Status)
        {
            bool res = false;
            try
            {
                SqlParameter[] param =
                        {
                            new SqlParameter ("@id",Id),
                            new SqlParameter ("@Status",Status),
                            new SqlParameter ("@Option","changeStatus"),
                        };

                res = DAL.DAL.ExecuteNonQuery("USP_InsertUpdateAuthor", CommandType.StoredProcedure, param);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        #endregion

        #region Use for Get Author List
        /// <summary>
        /// Use for Get Author List
        /// </summary>
        /// <returns></returns>
        public List<AuthorMasterList> GetAuthorList()
        {
            try
            {
                //MasterListModel model = new MasterListModel();
                List<AuthorMasterList> authorMasters = new List<AuthorMasterList>();
                DataSet ds = DAL.DAL.GenerateDataSet("USP_GetAuthorMasterList", CommandType.StoredProcedure, null);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    authorMasters.Add(
                        new AuthorMasterList
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Name = Convert.ToString(dr["Name"]),
                            Status=Convert.ToBoolean(dr["Status"])
                        }
                        );
                }

                //model.AuthorList = authorMasters;

                return authorMasters;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Use Add Author
        /// <summary>
        /// Use Add Author
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddAuthor(AuthorMasterList model)
        {
            bool result = false;
            //string option = "insert";
            
            try
            {
                string filname = "";
                if (model.Image != null && model.Image.ContentLength > 0)
                {
                    string path = HttpContext.Current.Server.MapPath("~/UploadedAuthorImages/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    filname = System.DateTime.Now.ToString("ddMMyy") + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + System.DateTime.Now.Millisecond.ToString() + Path.GetFileName(model.Image.FileName);
                    model.Image.SaveAs(path + filname);
                }
                SqlParameter[] param =
                        {  // new SqlParameter ("@option", option),
                            new SqlParameter("Id",model.Id),
                            new SqlParameter ("@Name", model.Name),
                            new SqlParameter ("@Image",(filname)==""?model.imageFile:filname),
                            new SqlParameter ("@Description", model.Description),
                            new SqlParameter ("@Profile", model.Profile),
                            new SqlParameter ("@Status", model.Status),
                            new SqlParameter ("@Designation", model.Designation),
                            new SqlParameter ("@FacebookUrl", model.FacebookUrl),
                            new SqlParameter ("@TwitterUrl", model.TwitterUrl),
                            new SqlParameter ("@LinkedInUrl", model.LinkedInUrl),
                            new SqlParameter ("@AboutAuthor", model.AboutAuthor),
                            new SqlParameter ("@option",model.Id==0?"insert":"UpdateAuthor"),

                        };
                result = DAL.DAL.ExecuteNonQuery("USP_InsertUpdateAuthor", CommandType.StoredProcedure, param);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion
        #region Use for Edit Blog
        /// <summary>
        /// Use for Edit Blog
        /// </summary>
        /// <returns></returns>
        public AuthorMasterList EditAuthor(int Id)
        {
            try
            {
                AuthorMasterList model = new AuthorMasterList();
                SqlParameter[] param =
               {
                    new SqlParameter ("@id", Id),
                    new SqlParameter ("@Option", "update"),
                };

                DataSet ds = DAL.DAL.GenerateDataSet("USP_InsertUpdateAuthor", CommandType.StoredProcedure, param);
               // DataTable dt = ds.Tables[0];
               // dt = dt.Rows
               //.Cast<DataRow>()
               //.Where(row => !row.ItemArray.All(f => f is DBNull))
               //.CopyToDataTable();
                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {
                    model.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
                    model.Name = Convert.ToString(ds.Tables[0].Rows[0]["Name"]);
                    model.imageFile = Convert.ToString(ds.Tables[0].Rows[0]["Image"]);
                    model.Description = Convert.ToString(ds.Tables[0].Rows[0]["Description"]);
                    model.Profile = Convert.ToString(ds.Tables[0].Rows[0]["Profile"]);
                    model.Status = Convert.ToBoolean(ds.Tables[0].Rows[0]["Status"]);
                    model.Designation = Convert.ToString(ds.Tables[0].Rows[0]["Designation"]);
                    model.FacebookUrl = Convert.ToString(ds.Tables[0].Rows[0]["FacebookUrl"]);
                    model.TwitterUrl = Convert.ToString(ds.Tables[0].Rows[0]["TwitterUrl"]);
                    model.LinkedInUrl = Convert.ToString(ds.Tables[0].Rows[0]["LinkedInUrl"]);
                    model.AboutAuthor = Convert.ToString(ds.Tables[0].Rows[0]["AboutAuthor"]);
                }
                //model.AuthorList = GetAuthorList();
                return model;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
        #region Use for Get Category List
        /// <summary>
        /// Use for Get Category List
        /// </summary>
        /// <returns></returns>
        public List<CategoryMasterList> GetCategoryList()
        {
            try
            {
                List<CategoryMasterList> categoryMasters = new List<CategoryMasterList>();
                DataSet ds = DAL.DAL.GenerateDataSet("USP_GetCategoryMasterList", CommandType.StoredProcedure, null);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    categoryMasters.Add(
                        new CategoryMasterList
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Name = Convert.ToString(dr["Name"]),
                            Status=Convert.ToBoolean(dr["Status"]),
                            
                        }
                        );
                }
                return categoryMasters;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region  Use for Change Category Status
        /// <summary>
        /// Use for Change Category Status
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public bool ChangeCategoryStatus(int Id, int Status)
        {
            bool res = false;
            try
            {
                SqlParameter[] param =
                        {
                            new SqlParameter ("@id",Id),
                            new SqlParameter ("@Status",Status),
                            new SqlParameter ("@Option","changeStatus"),
                        };

                res = DAL.DAL.ExecuteNonQuery("USP_InsertUpdateCategory", CommandType.StoredProcedure, param);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        #endregion

        #region Use for Edit Blog
        /// <summary>
        /// Use for Edit Blog
        /// </summary>
        /// <returns></returns>
        public AddBlogModel EditBlog(int Id)
        {
            try
            {
                AddBlogModel model = new AddBlogModel();
                SqlParameter[] param =
               {
                    new SqlParameter ("@id", Id),
                    new SqlParameter ("@action", "update"),
                };

                DataSet ds = DAL.DAL.GenerateDataSet("USP_InsertUpdateBlog", CommandType.StoredProcedure, param);

                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {
                    model.id = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
                    model.title = Convert.ToString(ds.Tables[0].Rows[0]["title"]);
                    model.key = Convert.ToString(ds.Tables[0].Rows[0]["key"]);
                    model.metaDescription = Convert.ToString(ds.Tables[0].Rows[0]["metaDescription"]);
                    model.metaKeywords = Convert.ToString(ds.Tables[0].Rows[0]["metaKeywords"]);
                    model.shortDesc = Convert.ToString(ds.Tables[0].Rows[0]["shortDesc"]);
                    model.authorId = Convert.ToString(ds.Tables[0].Rows[0]["authorId"]) == "" ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0]["authorId"]);
                    model.imageFile = Convert.ToString(ds.Tables[0].Rows[0]["imagePath"]);
                    model.categoryId = Convert.ToString(ds.Tables[0].Rows[0]["categoryId"]) == "" ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0]["categoryId"]);
                    model.tags = Convert.ToString(ds.Tables[0].Rows[0]["tags"]);
                    model.imageTag = Convert.ToString(ds.Tables[0].Rows[0]["imageTag"]);
                    model.description = Convert.ToString(ds.Tables[0].Rows[0]["description"]);
                    model.status = Convert.ToBoolean(ds.Tables[0].Rows[0]["status"]);
                    model.createdDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["createdDate"]);
                }
                model.AuthorList = GetAuthorList();
                model.CategoryList = GetCategoryList();

                return model;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Use for add New Blog 
        /// <summary>
        /// Use for add New Blog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public dynamic AddNewBlog(AddBlogModel model)
        {
            bool result = false;
            try
            {
                 
                string filname = "";
                if (model.imagePath != null && model.imagePath.ContentLength > 0)
                {
                    string path = HttpContext.Current.Server.MapPath("~/UploadedBlogImages/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    filname = System.DateTime.Now.ToString("ddMMyy") + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + System.DateTime.Now.Millisecond.ToString() + Path.GetFileName(model.imagePath.FileName);
                    model.imagePath.SaveAs(path + filname);
                }
                
                SqlParameter[] param =
                        {
                            new SqlParameter ("@id", model.id),
                            new SqlParameter ("@title", model.title),
                            new SqlParameter ("@key",model.key),
                            new SqlParameter ("@metaDescription",model.metaDescription),
                            new SqlParameter ("@metaKeywords",model.metaKeywords),
                            new SqlParameter ("@shortDesc",model.shortDesc),
                            new SqlParameter ("@authorId",model.authorId),
                            new SqlParameter ("@imagePath",(filname)==""?model.imageFile:filname),
                            new SqlParameter ("@categoryId",model.categoryId),
                            new SqlParameter ("@tags",model.tags),
                            new SqlParameter ("@imageTag",model.imageTag),
                            new SqlParameter ("@description",model.description),
                            
                            new SqlParameter ("@action",model.id==0?"insert":"updateblog"),
                        };
                result = DAL.DAL.ExecuteNonQuery("USP_InsertUpdateBlog", CommandType.StoredProcedure, param);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Use for Get Blog List
        /// <summary>
        /// Use for Get Blog List
        /// </summary>
        /// <returns></returns>
        public List<AddBlogModel> GetBlogList()
        {
            List<AddBlogModel> addBlogs = new List<AddBlogModel>();
            try
            {
                SqlParameter[] param =
                        {
                            new SqlParameter ("@action","select"),
                        };

                DataSet ds = DAL.DAL.GenerateDataSet("USP_InsertUpdateBlog", CommandType.StoredProcedure, param);
              
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    addBlogs.Add(
                        new AddBlogModel
                        {
                            id = Convert.ToInt32(dr["id"]),
                            title = Convert.ToString(dr["title"]),
                            key = Convert.ToString(dr["key"]),
                            metaDescription = Convert.ToString(dr["metaDescription"]),
                            metaKeywords = Convert.ToString(dr["metaKeywords"]),
                            shortDesc = Convert.ToString(dr["shortDesc"]),
                            authorId = Convert.ToString(dr["authorId"]) == "" ?0 : Convert.ToInt32(dr["authorId"]),
                            imageFile = Convert.ToString(dr["imagePath"]),
                            categoryId = Convert.ToString(dr["categoryId"]) == "" ?0 : Convert.ToInt32(dr["categoryId"]),   
                            tags = Convert.ToString(dr["tags"]),
                            imageTag = Convert.ToString(dr["imageTag"]),
                            description = Convert.ToString(dr["description"]),
                            status = Convert.ToBoolean(dr["status"]),
                            createdDate = Convert.ToDateTime(dr["createdDate"]),
                            author = Convert.ToString(dr["author"]),
                        }
                        );
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return addBlogs;
        }
        #endregion

        #region Use for Get Category View Data
        /// <summary>
        /// Use for Get Category View Data
        /// </summary>
        /// <returns></returns>
        public List<CategoryMasterList> GetCategoryViewData()
        {
            try
            {
                List<CategoryMasterList> categoryMasters = new List<CategoryMasterList>();
                DataSet ds = DAL.DAL.GenerateDataSet("USP_GetCategoryMasterData", CommandType.StoredProcedure, null);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    categoryMasters.Add(
                        new CategoryMasterList
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Name = Convert.ToString(dr["Name"]),
                            Status = Convert.ToBoolean(dr["Status"]),

                        }
                        );
                }

                //model.CategoryList = categoryMasters;

                return categoryMasters;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Use for Get Author View Data
        /// <summary>
        /// Use for Get Author View Data
        /// </summary>
        /// <returns></returns>
        public List<AuthorMasterList> GetAuthorViewData()
        {
            try
            {
                //MasterListModel model = new MasterListModel();
                List<AuthorMasterList> authorMasters = new List<AuthorMasterList>();
                DataSet ds = DAL.DAL.GenerateDataSet("USP_GetAuthorMasterData", CommandType.StoredProcedure, null);
                DataTable dt = ds.Tables[0].Rows
                .Cast<DataRow>()
                .Where(row => !row.ItemArray.All(f => f is DBNull ||
                                     string.IsNullOrWhiteSpace(f as string)))
                .CopyToDataTable();
                foreach (DataRow dr in dt.Rows)
                {
                        authorMasters.Add(
                        new AuthorMasterList
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Name = Convert.ToString(dr["Name"]),
                            Status = Convert.ToBoolean(dr["Status"]),
                            imageFile = Convert.ToString(dr["Image"]),
                            Designation = Convert.ToString(dr["Designation"]),
                            FacebookUrl = Convert.ToString(dr["FacebookUrl"]),
                            TwitterUrl = Convert.ToString(dr["TwitterUrl"]),
                            LinkedInUrl = Convert.ToString(dr["LinkedInUrl"]),
                            AboutAuthor = Convert.ToString(dr["AboutAuthor"])
                        }
                        );
                }
                return authorMasters;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

    }
}
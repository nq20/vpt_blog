﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Vpt_Blogs.Models;

namespace Vpt_Blogs.Service
{
    public class VptBlogView
    {
        #region Use for Get Blog List
        /// <summary>
        /// Use for Get Blog List
        /// </summary>
        /// <returns></returns>
        public GetBlogListModel GetBlogList()
        {
            try
            {
            GetBlogListModel model = new GetBlogListModel();
            List<GetBlogListModel> blogListModels = new List<GetBlogListModel>();
            DataSet ds = DAL.DAL.GenerateDataSet("USP_GetBlogListData", CommandType.StoredProcedure, null);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                blogListModels.Add(
                    new GetBlogListModel
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        CreatedDate = Convert.ToDateTime(dr["CreatedDate"]),
                        Author = Convert.ToString(dr["Author"]),
                        Title = Convert.ToString(dr["Title"]),
                        ShortDesc = Convert.ToString(dr["ShortDesc"]),
                        ImagePath = Convert.ToString(dr["ImagePath"]),
                        Urlkey = Convert.ToString(dr["Urlkey"]),
                    }
                    );
            }

            model.blogListModels = blogListModels;

            return model;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Use for Get Blog Details
        /// <summary>
        /// Use for Get Blog Details
        /// </summary>
        /// <returns></returns>
        public BlogDetailsModel GetBlogDetails(string UrlKey)
        {
            try
            {
            BlogDetailsModel blogDetails = new BlogDetailsModel();
            List<PopularPosts> posts = new List<PopularPosts>();
            List<PopularCategory> categories = new List<PopularCategory>();
            List<PopularTags> tags = new List<PopularTags>();
             //List<AuthorMasterList> AuthorList = new List<AuthorMasterList>();

        SqlParameter[] param =
                {
                    new SqlParameter ("@UrlKey", UrlKey),
                };

            DataSet ds = DAL.DAL.GenerateDataSet("USP_GetBlogDetailsData", CommandType.StoredProcedure, param);
            
            if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
            {
                blogDetails.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
                blogDetails.CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["CreatedDate"]);
                blogDetails.Author = Convert.ToString(ds.Tables[0].Rows[0]["Author"]);
                blogDetails.Title = Convert.ToString(ds.Tables[0].Rows[0]["Title"]);
                blogDetails.Description = Convert.ToString(ds.Tables[0].Rows[0]["Description"]);
                    blogDetails.ShortDesc = Convert.ToString(ds.Tables[0].Rows[0]["ShortDesc"]);
                    blogDetails.ImagePath = Convert.ToString(ds.Tables[0].Rows[0]["ImagePath"]);
                blogDetails.KeywordContent = Convert.ToString(ds.Tables[0].Rows[0]["KeywordContent"]);
                blogDetails.MetaContent = Convert.ToString(ds.Tables[0].Rows[0]["MetaContent"]);
                blogDetails.AltTags = Convert.ToString(ds.Tables[0].Rows[0]["AltTags"]);
                blogDetails.Designation = Convert.ToString(ds.Tables[0].Rows[0]["Designation"]);
                    blogDetails.FacebookUrl = Convert.ToString(ds.Tables[0].Rows[0]["FacebookUrl"]);
                    blogDetails.TwitterUrl = Convert.ToString(ds.Tables[0].Rows[0]["TwitterUrl"]);
                    blogDetails.LinkedInUrl = Convert.ToString(ds.Tables[0].Rows[0]["LinkedInUrl"]);
                    blogDetails.AboutAuthor = Convert.ToString(ds.Tables[0].Rows[0]["AboutAuthor"]);
                    blogDetails.imageFile = Convert.ToString(ds.Tables[0].Rows[0]["Image"]);
                }

                if (ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        posts.Add(
                            new PopularPosts
                            {
                                ImagePath = Convert.ToString(dr["ImagePath"]),
                                Title = Convert.ToString(dr["Title"]),
                                Tags = Convert.ToString(dr["Tags"]),
                                Urlkey = Convert.ToString(dr["Urlkey"]),
                            }
                            );
                    }
                }

                if (ds.Tables[2].Rows != null && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        categories.Add(
                            new PopularCategory
                            {     
                                Category = Convert.ToString(dr["Category"]),
                            }
                            );
                    }
                }

                if (ds.Tables[3].Rows != null && ds.Tables[3].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[3].Rows)
                    {
                        tags.Add(
                            new PopularTags
                            {
                                Tag = Convert.ToString(dr["Tags"]),
                            }
                            );
                    }
                }
                

                blogDetails.popularPosts = posts;
                blogDetails.popularCategories = categories;
                blogDetails.popularTags = tags;
                return blogDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Use for Change Blog status
        /// <summary>
        /// Use for Change Blog status
        /// </summary>
        /// <returns></returns>
        public bool ChangeBlogStatus(int Id,int Status)
        {
            bool res = false;
            try
            {
                SqlParameter[] param =
                        {
                            new SqlParameter ("@id",Id),
                            new SqlParameter ("@status",Status),
                            new SqlParameter ("@action","changeStatus"),
                        };

                res = DAL.DAL.ExecuteNonQuery("USP_InsertUpdateBlog", CommandType.StoredProcedure, param);   

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        #endregion

    }
}
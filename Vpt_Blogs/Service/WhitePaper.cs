﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Vpt_Blogs.Models;

namespace Vpt_Blogs.Service
{
    public class WhitePaper
    {
        #region Use for Get WhitePaper List
        /// <summary>
        /// Use for Get WhitePaper List
        /// </summary>
        /// <returns></returns>
        public WhitePaperModel GetWhitePaperList()
        {
            try
            {
                WhitePaperModel model = new WhitePaperModel();
                List<WhitePaperModel> WhitePaperModel = new List<WhitePaperModel>();
                DataSet ds = DAL.DAL.GenerateDataSet("USP_GetWhitePaper", CommandType.StoredProcedure, null);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    WhitePaperModel.Add(
                        new WhitePaperModel
                        {
                            Id = Convert.ToInt32(dr["Id"]),

                        }
                        );
                }

                model.WhitePaperListModels = WhitePaperModel;

                return model;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
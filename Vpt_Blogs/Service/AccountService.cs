﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using Vpt_Blogs.Models;

namespace Vpt_Blogs.Service
{
    public class AccountService
    {
        #region Job application
        /// <summary>
        /// Job application
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public bool SendJobApplication(JobApplicationModel model)
        {
            bool result = false;
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("no-reply@viruspositive.com");
                mail.To.Add("hr@viruspositive.com");
                mail.Subject = "Job Application" + $"({model.fullName})";
                string fname = model.fullName.Split(' ').ToList()[0];
                mail.Body = $"Job Apllication " +
                            $"{Environment.NewLine} {Environment.NewLine} " +
                            $"Applicant Name: {model.fullName} {Environment.NewLine}" +
                            $"Email: {model.emailAddress} {Environment.NewLine}" +
                            $"Mobile: {model.mobileNumber} {Environment.NewLine}" +
                            $"Profile: {model.position} {Environment.NewLine}" +
                            $"LinkedIn Profile: {model.linkedinProfile} {Environment.NewLine}" +
                            $"Details: {model.details} {Environment.NewLine}{Environment.NewLine}";
                //Attachments
                string extension = GetFileExtension(model.resume);
                var resume = model.resume.Split(',').ToList<string>();
                var bytes = Convert.FromBase64String(resume[1]);
                Stream stream = new MemoryStream(bytes);
                mail.Attachments.Add(new Attachment(stream, model.fullName + extension));
                //Attechments

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@viruspositive.com", "virus@123");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        private static string GetFileExtension(string base64String)
        {
            string[] strings = base64String.Split(',');
            string extension;
            switch (strings[0])
            {
                case "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64":
                    extension = ".docx";
                    break;
                case "data:application/pdf;base64":
                    extension = ".pdf";
                    break;
                default:
                    extension = ".txt";
                    break;
            }
            return extension;
        }

        #endregion
    }
}
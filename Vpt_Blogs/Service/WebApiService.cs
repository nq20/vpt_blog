﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Vpt_Blogs.Models;

namespace Vpt_Blogs.Service
{

    public class WebApiService
    {
        #region Get Web Blog List
        /// <summary>
        /// Get Web Blog List
        /// </summary>
        /// <returns></returns>
        public List<GetBlogListModel> GetWebBlogList()
        {
            try
            {
                //GetBlogListModel model = new GetBlogListModel();
                List<GetBlogListModel> blogListModels = new List<GetBlogListModel>();
                DataSet ds = DAL.DAL.GenerateDataSet("USP_GetWebBlogList", CommandType.StoredProcedure, null);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    blogListModels.Add(
                        new GetBlogListModel
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CreatedDate = Convert.ToDateTime(dr["CreatedDate"]),
                            Author = Convert.ToString(dr["Author"]),
                            Title = Convert.ToString(dr["Title"]),
                            ShortDesc = Convert.ToString(dr["ShortDesc"]),
                            ImagePath = Convert.ToString(dr["ImagePath"]),
                            Urlkey = Convert.ToString(dr["Urlkey"]),
                        }
                        );
                }

                

                return blogListModels;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vpt_Blogs.Models;
using Vpt_Blogs.Service;

namespace Vpt_Blogs.Controllers
{
    [RoutePrefix("blog")]
    public class blogController : Controller
    {
        VptBlogView _obj = new VptBlogView();

        // GET: Home
        #region Use For Get Blog List
        /// <summary>
        /// Use For Get Blog List
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            GetBlogListModel blogListModel = new GetBlogListModel();
            blogListModel = _obj.GetBlogList();
            return View(blogListModel);
        }
        #endregion

    }
}
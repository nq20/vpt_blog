﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;
using Vpt_Blogs.App_Start;
using Vpt_Blogs.Models;
using Vpt_Blogs.Service;

namespace Vpt_Blogs.Controllers
{
    [RoutePrefix("api/Web")]
    [EnableCors(origins: "https://viruspositive.com", headers: "accept,content-type,origin,x-my-header", methods: "*")]
   // [EnableCors(origins: "'https://localhost:4200','http://localhost:4200'", headers: "accept,content-type,origin,x-my-header", methods: "*")]
    public class WebController : ApiController
    {
        WebApiService _obj = new WebApiService();

        #region use for Get Web Blog List
        /// <summary>
        /// Get Web Blog List
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetWebBlogList")]
        public dynamic GetWebBlogList()
        {
            try
            {
                ReturnModel _returnModel = new ReturnModel();
                _returnModel.data = _obj.GetWebBlogList();
                var result = Newtonsoft.Json.JsonConvert.SerializeObject(_returnModel);
                return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        //Email configuration
        #region for send vpt emails
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[AllowAnonymous]
        [Route("SendEmail")]
        public dynamic SendEmail(VptSendMail model)
        {
            try
            {
                var result = "";
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("no-reply@viruspositive.com");
                mail.To.Add("sales@viruspositive.com");
                mail.Subject = model.subject + " (Contact Enquiry)";
                mail.Body = "Name: " + " " + model.fullName + Environment.NewLine + " Mobile: " + model.mobileNumber + Environment.NewLine + " Email: " + model.emailAddress + Environment.NewLine + "Company Name: " + model.companyName + Environment.NewLine + "Designation: " + model.designation + Environment.NewLine + "Country: " + model.countryName + Environment.NewLine + "Message: " + model.message;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@viruspositive.com", "virus@123");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                result = JsonConvert.SerializeObject(
                           new
                           {
                               message = "Success",
                           });
                SendThankyouEmail(model);
                return result;
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        #endregion

        #region for vptemail Document Email
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[AllowAnonymous]
        [Route("DocumentRequestEmail")]
        public dynamic VptDocumentRequestEmail(VptSendMail model)
        {
            try
            {
                var result = "";
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("no-reply@viruspositive.com");
                mail.To.Add("sales@viruspositive.com");
                mail.Subject = model.subject;
                mail.Body = "You have received WhitePaper Document request from below user" + Environment.NewLine + "Name: " + " " + model.fullName + Environment.NewLine + " Mobile: " + model.mobileNumber + Environment.NewLine + " Email: " + model.emailAddress + Environment.NewLine + "  Company: " + model.message;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@viruspositive.com", "virus@123");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                result = JsonConvert.SerializeObject(
                           new
                           {
                               message = "Success",
                           });
                return result;
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        #endregion

        #region for Vpt Subscription Email
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[AllowAnonymous]
        [Route("SubscriptionEmail")]
        public dynamic VptSubscriptionEmail(VptSendMail model)
        {
            try
            {
                var result = "";
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("no-reply@viruspositive.com");
                mail.To.Add("sales@viruspositive.com");
                mail.Subject = model.subject;
                mail.Body = "You have received subscription request from below user" + Environment.NewLine + " Email: " + model.emailAddress; ;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@viruspositive.com", "virus@123");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                result = JsonConvert.SerializeObject(
                           new
                           {
                               message = "Success",
                           });
                return result;
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        #endregion

        #region Thankyou email to visitor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void SendThankyouEmail(VptSendMail model)
        {
            try
            {
                var result = "";
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("no-reply@viruspositive.com");
                mail.To.Add(model.emailAddress);
                mail.Subject = model.subject;
                string fname = model.fullName.Split(' ').ToList()[0];
                mail.Body = $"Dear {fname}, {Environment.NewLine} {Environment.NewLine} Thank you for writing to us! We will get back to you within 24 business hours. {Environment.NewLine}{Environment.NewLine} Should you have any questions, please write to us at sales@viruspositive.com. {Environment.NewLine} {Environment.NewLine} Thank you, {Environment.NewLine} Sales Team {Environment.NewLine} Virus Positive Technologies";
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@viruspositive.com", "virus@123");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                result = JsonConvert.SerializeObject(
                           new
                           {
                               message = "Success",
                           });

            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        //end email
        #region Job application
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[AllowAnonymous]
        [Route("JobApplication")]
        public dynamic JobApplication([FromBody] JobApplicationModel model)
        {
            try
            {
                var service = new AccountService();
                ReturnModel _returnModel = new ReturnModel();
                _returnModel.data = service.SendJobApplication(model);
                if (_returnModel.data == true)
                {
                    try
                    {
                        var result = "";
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                        mail.From = new MailAddress("no-reply@viruspositive.com");
                        mail.To.Add(model.emailAddress);
                        string fname = model.fullName.Split(' ').ToList()[0];
                        mail.Subject = $"Thank You- {fname} | {model.position}";
                        mail.Body = $"Dear {fname}, {Environment.NewLine} {Environment.NewLine}We are currently reviewing your application and will connect with you if your profile is shortlisted.{Environment.NewLine} {Environment.NewLine}Thank you, {Environment.NewLine}HR Team {Environment.NewLine}Virus Positive Technologies";
                        SmtpServer.Port = 587;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@viruspositive.com", "virus@123");
                        SmtpServer.EnableSsl = true;
                        SmtpServer.Send(mail);
                        result = JsonConvert.SerializeObject(
                                   new
                                   {
                                       message = "Success",
                                   });

                    }
                    catch (Exception ex)
                    {

                    }
                }
                return Ok(_returnModel);
            }
            catch (Exception ex)
            {
                ReturnModel _returnModel = new ReturnModel();
                _returnModel.data = null;
                _returnModel.message = ex.Message;
                return BadRequest(_returnModel.message);
            }
        }
        #endregion
    }
}

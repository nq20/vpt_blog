﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Vpt_Blogs.Filters;
using Vpt_Blogs.Models;
using Vpt_Blogs.Service;

namespace Vpt_Blogs.Controllers
{

    public class AccountController : Controller
    {
        MasterService _mObj = new MasterService();
        VptBlogView _obj = new VptBlogView();
        public AccountController()
        {
            TempData["Value"] = null;
        }

        #region Use for get Login view
        /// <summary>
        /// Use for get Login view
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ValidateSessionFilter(false)]
        public ActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View("Login");
        }
        #endregion

        #region Use for Login Post 
        /// <summary>
        /// Use for Login Post
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ValidateSessionFilter(false)]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var user = new UserModel();
            var response = new Response { Success = false };
            clsLogin clsAccount = new clsLogin();
            if (ModelState.IsValid)
            {
                response = clsAccount.IsValid(model.UserName, model.Password);
                user = (UserModel)response.Data;
                if (response.Success == true && user != null)
                {
                    Session["userId"] = user.Id;
                    Session["username"] = user.Email;
                    Session["roleId"] = user.RoleId;
                    Session["loginTime"] = DateTime.Now;

                    if (!string.IsNullOrEmpty(returnUrl) && !string.Equals(returnUrl, "/") && Url.IsLocalUrl(returnUrl))
                        return RedirectToLocal(returnUrl);

                    return RedirectToAction(nameof(blogController.Index), "Account");
                }
                else
                {
                    ModelState.AddModelError("", response.ErrorMessage.ToString());
                }
            }
            return View("Login", model);
        }
        #endregion

        #region Use For Redirect To Local
        /// <summary>
        /// Use For Redirect To Local
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Account");
        }
        #endregion

        #region Use For LogOut
        /// <summary>
        /// Use For LogOut
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "blog");
        }
        #endregion

        #region Use For Get Dashboard View
        /// <summary>
        /// Use For Get Dashboard View
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateSessionFilter(true)]
        public ActionResult Index()
        {
            List<AddBlogModel> blogModels = new List<AddBlogModel>();
            blogModels = _mObj.GetBlogList();
            return View(blogModels);
        }
        #endregion

        #region Use For Create Blog View
        /// <summary>
        /// Use For Create Blog View
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateSessionFilter(true)]
        [HttpGet]
        public ActionResult CreateBlog()
        {
            AddBlogModel addBlog = new AddBlogModel();
            if (TempData["Value"] != null)
            {
                addBlog = (AddBlogModel)TempData["Value"];
            }
            else
            {
                
                addBlog.AuthorList = _mObj.GetAuthorList();
                addBlog.CategoryList = _mObj.GetCategoryList();
            }
            return View(addBlog);
        }
        #endregion

        #region Use For Edit Blog
        /// <summary>
        /// Use For Edit Blog
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            AddBlogModel addBlog = new AddBlogModel();
            addBlog = _mObj.EditBlog(Id);
            TempData["Value"] = addBlog;
            return RedirectToAction("CreateBlog");
        }
        #endregion

        #region Use for add Blog post method
        /// <summary>
        /// Use for add Blog post method
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateBlog(AddBlogModel model)
        {
            if (ModelState.IsValid)
            {
                bool result = _mObj.AddNewBlog(model);
                
                if (result)
                {
                    if (model.id == 0)
                    {
                        TempData["success"] = "Blog Created Successfully .";
                    }
                    else {
                        TempData["success"] = "Blog Updated Successfully .";
                    }
                    
                }
                return RedirectToAction("CreateBlog");
            }
            else
            {
                TempData["danger"] = "Some fields are required .";
            }
            model.AuthorList = _mObj.GetAuthorList();
            model.CategoryList = _mObj.GetCategoryList();
            return View(model);

        }
        #endregion

        #region Use For Add Category View
        /// <summary>
        /// Use For Add Category View
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateSessionFilter(true)]
        [HttpGet]
        public ActionResult AddCategory()
        {
            GetVptBlogModel model = new GetVptBlogModel();
            model.CategoryList = _mObj.GetCategoryViewData();

            return View(model);
        }
        #endregion

        #region Use For Add Category Post 
        /// <summary>
        /// Use For Add Category Post 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddCategory(GetVptBlogModel model)
        {
            if (ModelState.IsValid)
            {
                _mObj.AddCategory(model);
                return RedirectToAction("AddCategory", model);
            }
            else
            {
                model.CategoryList = _mObj.GetCategoryViewData();
                return View(model);
            } 

        }
        #endregion

        #region Use for Change Category Status
        /// <summary>
        /// Use for Change Category Status
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public bool ChangeCategoryStatus(int Id, int Status)
        {
            bool res = _mObj.ChangeCategoryStatus(Id, Status);
            return res;
        }
        #endregion

        #region Use For Add Author View
        /// <summary>
        /// Use For Add Author View
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateSessionFilter(true)]
        [HttpGet]
        public ActionResult AddAuthor()
        {
            AuthorMasterList model = new AuthorMasterList();
            if (TempData["Value"] != null)
            {
                model = (AuthorMasterList)TempData["Value"];
                return View(model);
            }
            
            model.AuthorList = _mObj.GetAuthorViewData();
            return View(model);
        }
        #endregion

        #region Use for Add Author post
        /// <summary>
        /// Use for Add Author post
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddAuthor(AuthorMasterList model)
        {
            if (ModelState.IsValid)
            {
                bool result = _mObj.AddAuthor(model);

                if (result)
                {
                    if (model.Id == 0)
                    {
                        TempData["success"] = "Author Created Successfully .";
                    }
                    else
                    {
                        TempData["success"] = "Author Updated Successfully .";
                    }

                }
                return RedirectToAction("AddAuthor");
            }
            else
            {
                TempData["danger"] = "Some fields are required .";
            }
            return View(model);
        }
        #endregion

        #region Use For Edit Author
        /// <summary>
        /// Use For Edit Author
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditAuthor(int Id)
        {
            
            AuthorMasterList AddAuthor = new AuthorMasterList();
            AddAuthor = _mObj.EditAuthor(Id);
            TempData["Value"] = AddAuthor; 
            
            return RedirectToAction("AddAuthor");
        }
        
        #endregion
        #region Use for Change Author Status
        /// <summary>
        /// Use for Change Author Status
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public bool ChangeAuthorStatus(int Id, int Status)
        {
            bool res = _mObj.ChangeAuthorStatus(Id, Status);
            return res;
        }

        #endregion

        #region Use For change blog Status
        /// <summary>
        /// Use For change blog Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public bool ChangeBlogStatus(int Id, int Status)
        {
            bool res = _obj.ChangeBlogStatus(Id, Status);
            return res;
        }
        #endregion
        
    }
}
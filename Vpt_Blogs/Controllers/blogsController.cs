﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vpt_Blogs.Models;
using Vpt_Blogs.Service;

namespace Vpt_Blogs.Controllers
{
    [RoutePrefix("blogs")]
    [OutputCache(Duration = 300, VaryByParam = "none")]
    public class blogsController : Controller
    {
        //private string hostName = "http://localhost:44319/blogs/";
        private string hostName = "https://viruspositive.com/resources/blogs/";
        VptBlogView _obj = new VptBlogView();
        // GET: blogs
        #region Use for Get BlogDetails On Website
        /// <summary>
        /// Use for Get BlogDetails On Website
        /// </summary>
        /// <param name="UrlKey"></param>
        /// <returns></returns>
        [Route("{UrlKey}")]
        public ActionResult Index(string UrlKey)
        {
            if (UrlKey.Contains("_")) 
            {
                return RedirectPermanent(hostName + GetOrignalKey(UrlKey));
            }
            BlogDetailsModel blogListModel = new BlogDetailsModel();
            blogListModel = _obj.GetBlogDetails(UrlKey);
            if (blogListModel.Title == null)
            {
               return RedirectToAction("Index", "blog");
            }
            else
            {
                return View(blogListModel);
            }
        }

        private string GetOrignalKey(string key="") 
        {
            string orignalKey = string.Empty;
            //here will  be the code to get orignal key
            orignalKey=BlogKeysList().Find(x => x.OldKey?.Trim()?.ToLower() == key?.Trim()?.ToLower())?.CurrentKey;
            orignalKey = !string.IsNullOrEmpty(orignalKey) ? orignalKey : key.Replace('_','-');
            return orignalKey;
        }

        private List<BlogKeyModel> BlogKeysList() 
        {
            var result = new List<BlogKeyModel>();
            result.Add(new BlogKeyModel {
            OldKey= "cookie_stuffing:_the_blackhat_online_marketing_technique",
            CurrentKey= "cookie-stuffing-the-blackhat-online-marketing-technique"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "how_artificial_intelligence_is_enabling_software_development",
                CurrentKey = "how-artificial-intelligence-is-enabling-software-development"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "malvertising:_the_darker_side",
                CurrentKey = "malvertising-the-darker-side"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "5_key_aspects_to_consider_before_outsourcing_software_testing_services",
                CurrentKey = "5-key-aspects-to-consider-before-outsourcing-software-testing-services"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "5_key_practices_for_a_cio_to_lead_digital_transformation",
                CurrentKey = "5-key-practices-for-a-cio-to-lead-digital-transformation"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "5_things_to_consider_for_choosing_the_right_technology_partner",
                CurrentKey = "5-things-to-consider-for-choosing-the-right-technology-partner"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "email_support,_a_convenient_way_for_customers_to_connect_with_businesses",
                CurrentKey = "email-support-a-convenient-way-for-customers-to-connect-with-businesses"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "annual_appraisals_for_employees_marked_another_growth_story_of_vpt",
                CurrentKey = "annual-appraisals-for-employees-marked-another-growth-story-of-vpt"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "5_reasons_live_chat_support_is_critical_for_any_business_on_the_web",
                CurrentKey = "5-reasons-live-chat-support-is-critical-for-any-business-on-the-web"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "know_all_about_cookie_hijacking_and_its_impact_on_your_brand",
                CurrentKey = "cookie-hijacking-and-its-impact-on-your-brand"
            }); 

            result.Add(new BlogKeyModel
            {
                OldKey = "what_is_adware_round_tripping_?",
                CurrentKey = "what-is-adware-round-tripping"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "what_are_misleading_deals_?",
                CurrentKey = "what-are-misleading-deals"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "what_do_you_need_to_know_about_typosquatting?",
                CurrentKey = "what-do-you-need-to-know-about-typosquatting"
            });

            result.Add(new BlogKeyModel
            {
                OldKey = "malicious_bots_–_all_you_need_to_know",
                CurrentKey = "malicious-bots-all-you-need-to-know"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "outsourcing_tech_–_3_reasons_why_you_should_outsource_software_development_services?",
                CurrentKey = "outsourcing-tech-reasons-why-you-should-outsource-software-development-services"
            });
            result.Add(new BlogKeyModel
            {
                OldKey = "what_is_affiliate_fraud?",
                CurrentKey = "what-is-affiliate-fraud"
            });


            return result;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Vpt_Blogs.Filters
{
    public class ValidateSessionFilter: ActionFilterAttribute
    {
        public bool Disable { get; set; }

        public ValidateSessionFilter(bool disable)
        {
            Disable = disable;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Disable)
            {
                var sessionDict = HttpContext.Current.Session.GetEnumerator();
                var timeOut = TimeSpan.FromMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["SessionTimeOut"]));
                if (sessionDict != null)
                {
                    var loginTime = Convert.ToDateTime(HttpContext.Current.Session["loginTime"]);

                    if (loginTime.Add(timeOut) <= DateTime.Now)
                    {
                        var loginPath = new RouteValueDictionary(new
                        {
                            action = "Login",
                            controller = "Account",
                            returnUrl = HttpContext.Current.Request.Url.AbsolutePath
                        });

                        filterContext.Result = new RedirectToRouteResult(loginPath);
                    }
                }
                base.OnActionExecuting(filterContext);
            }

        }
    }
}